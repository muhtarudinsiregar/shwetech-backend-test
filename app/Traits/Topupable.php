<?php
namespace App\Traits;

use Illuminate\Support\Facades\DB;
use App\Topup;
use App\User;
/**
 *
 */
trait Topupable
{
    public function storeTopup($request)
    {
        $user = auth()->user();
        $isRecipientAgent = User::recipientIsAgent($request->recipient_user_id);

        if ($isRecipientAgent && !$user->isMasterAgent()) {
            return $this->response->error(trans('topup.error_is_not_master_agent'), 400);
        }

        return $this->storeProcess($request, $user);
    }

    public function storeProcess($request, $user)
    {
        $request->merge(['sender_user_id' => $user->id]);

        DB::transaction(
            function () use ($request, $user) {
                Topup::create($request->all());
                $user->increment('total_coin', $request->amount);
            }
        );

        return $this->response->created();
    }
}
