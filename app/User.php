<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    const MASTER_AGENT = 1;
    const AGENT = 2;
    const BUYER = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'level', 'parent_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }
    public function level() : int
    {
        return (int)$this->level;
    }

    public function isMasterAgent()
    {
        return $this->level() == self::MASTER_AGENT;
    }

    public function isParentUser(int $id)
    {
        return static::where('parent_id', $this->id)
            ->where('id', $id)
            ->exists();
    }

    public function scopeRecipientIsAgent($query, $recipientId)
    {
        return $query->find($recipientId)->isAgent();
    }

    public function isAgent()
    {
        return $this->level() == self::AGENT;
    }
}
