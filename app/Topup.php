<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topup extends Model
{
    protected $fillable = [
        'recipient_user_id',
        'amount',
        'sender_user_id'
    ];
}
