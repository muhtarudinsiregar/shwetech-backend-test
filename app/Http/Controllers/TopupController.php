<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Topup;
use App\Http\Requests\TopupRequest;
use App\Traits\Topupable;
use App\User;

class TopupController extends Controller
{
    use Topupable;

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function store(TopupRequest $request)
    {
        return $this->storeTopup($request);
    }
}
