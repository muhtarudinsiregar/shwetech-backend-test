<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;

class TopupControllerTest extends TestCase
{
    use DatabaseMigrations;
    const MASTER_AGENT = 1;
    const AGENT = 2;
    const BUYER = 3;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIsTopupSuccess()
    {
        $user = factory(User::class)->create(['level' => self::AGENT]);
        $recipient = $this->createUser(['level' => self::BUYER]);

        $response = $this->post(
            'api/v1/topup',
            [
                'amount' => 50000,
                'recipient_user_id' => 2
            ],
            $this->headers($user)
        );

        $response->assertSuccessful();
    }

    /** @test */
    public function master_agent_topup_to_agent()
    {
        $user = $this->createUser(['level' => self::MASTER_AGENT, ]);
        $recipient = $this->createUser(
            ['level' => self::AGENT, 'parent_id' => $user->id]
        );

        $response = $this->post(
            'api/v1/topup',
            [
                'amount' => 50000,
                'recipient_user_id' => 2
            ],
            $this->headers($user)
        );

        $response->assertSuccessful();
    }

    /** @test */
    public function it_should_400_when_buyer_topup_to_agent()
    {
        $user = $this->createUser(['level' => self::BUYER]);
        $recipient = $this->createUser(
            ['level' => self::AGENT]
        );

        $response = $this->post(
            'api/v1/topup',
            [
                'amount' => 50000,
                'recipient_user_id' => 2
            ],
            $this->headers($user)
        );

        $response->assertStatus(400)->assertJsonFragment(
            [
                'message' => trans('topup.error_is_not_master_agent')
            ]
        );
    }
}
