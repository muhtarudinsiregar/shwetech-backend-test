## Installation
* clone this repo
* run `composer install` in root project
* copy .env.example to .env
* setup your db config in .env
* run command `php artisan jwt:secret`
* run migrate `php artisan migrate:fresh --seed`

* Or run command `make`. if you have `make` command in your pc/laptop.
* run test `make test` or `vendor/bin/phpunit`

## Usage
* Get token : `localhost:8000/api/auth/login`

* request to endpoint who need authentication
```php
Authorization : Bearer <token>
```

## Endpoint

* `POST : api/auth/login`
    * untuk login, menggunakan email, password('secret') dari table user

* `POST : api/auth/logout`
    * masukkan token di auth bearer

* `POST : api/auth/register`
    * payload : `email`, `password` and `name`, `level`.

* `api/v1/topup`
    * payload : `int amount`, `int recipient_user_id`

---
My Laravel Skeleton : https://github.com/muhtarudinsiregar/laravel-rest-boilerplate

## Package
* [x] [dingo/api](https://github.com/dingo/api)
* [x] [tymondesigns/jwt-auth](https://github.com/tymondesigns/jwt-auth)
* [x] [mpociot/laravel-apidoc-generator](https://github.com/mpociot/laravel-apidoc-generator)
* [x] [barryvdh/laravel-cors](https://github.com/barryvdh/laravel-cors)
