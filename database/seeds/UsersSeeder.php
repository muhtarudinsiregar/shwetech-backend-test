<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $level = ['master_agent', 'agent', 'buyer'];

        // create dummy user
        for ($i = 0; $i < count($level); $i++) {
            factory(App\User::class)->states($level[$i])->create();
        }
    }
}
