<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(
    App\User::class,
    function (Faker $faker) {
        return [
            'name' => $faker->name,
            'email' => $faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'password' => 'secret', // secret
            'remember_token' => str_random(10),
            'level' => $faker->numberBetween(1, 3),
            'level' => $faker->numberBetween(1, 3)
        ];
    }
);

$factory->state(
    App\User::class,
    'master_agent',
    [
        'level' => 1,
    ]
);

$factory->state(
    App\User::class,
    'agent',
    [
        'level' => 2,
        'parent_id' => 1
    ]
);

$factory->state(
    App\User::class,
    'buyer',
    [
        'level' => 3,
    ]
);
