<?php

return [
    'error_is_not_master_agent' => 'Cannot topup, because you are not a master agent from recipient'
];
